Animals class
----------------
.. autoclass:: biosim.Fauna.Animals
    :members:

Herbivores class
-----------------
.. autoclass:: biosim.Fauna.Herbivores
    :members:

Carnivores class
-----------------
.. autoclass:: biosim.Fauna.Carnivores
    :members:
