Cell class
================
.. autoclass:: biosim.celle.Cell
    :members:

Lowland class
----------------
.. autoclass:: biosim.celle.Lowland
    :members:

Highland class
----------------
.. autoclass:: biosim.celle.Highland
    :members:

Desert class
----------------
.. autoclass:: biosim.celle.Desert
    :members:

Water class
----------------
.. autoclass:: biosim.celle.Water
    :members:
