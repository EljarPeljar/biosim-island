.. BIOSIM ISLAND documentation master file, created by
   sphinx-quickstart on Tue Jun 20 14:10:05 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BIOSIM ISLAND's documentation!
=========================================
This is a simulation of
 * an Island
 * with different landscapes (cells)
 * containing herbivores and carnivores as population.



.. toctree::
   :maxdepth: 3
   :caption: Contents:

   celle
   Islands
   Fauna
   simulation
   visualizattion



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
