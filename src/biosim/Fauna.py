"""
Module containing animal classes with their set of attributes.
"""

import random
import math


class Animals:
    """
    Parent class for Herbivores and Carnivores.

    Methods
    -------
    set_params: class method
    __init__
    moves
    has_moved
    reset_moved
    ages
    birth_weight
    gain_weight
    lose_weight
    fitness
    birth
    death

    """
    default_parameters = {}

    @classmethod
    def set_params(cls, new_params):
        """
        Updates parameters if needed

        :param new_params: dictionary of new parameters to be updated.
        :raises: ValueError if criterias aren't met.
        """
        # O(n^2 - to for loops) complexity
        for key in new_params:
            if key not in ('w_birth', 'sigma_birth', 'beta', 'eta',
                           'a_half', 'phi_age', 'w_half', 'phi_weight',
                           'mu', 'gamma', 'zeta', 'xi', 'omega', 'F', 'DeltaPhiMax'):
                raise KeyError('Invalid parameter name: ' + key)

        for params in new_params:
            if params in ('DeltaPhiMax', 'eta'):
                if not 0 < new_params[params]:
                    raise ValueError(params + ": Must be larger than zero")
            elif not 0 <= new_params[params]:
                raise ValueError(params, ": Must be larger or equal to zero")

        cls.default_parameters.update(new_params)

    def __init__(self, age=0, weight=None, fitness=None):
        """
        Initializes an animal object and calculates weight
        and fitness if nothing else is specified.

        :param age: Specified age of animal
        :param weight: specified weight of animal

        :raises: ValueError if negative params.
        """
        self.age = age
        self.weight = weight
        self.phitness = fitness
        self.moved = False

        if self.weight is None:
            self.weight = self.birth_weight()

        if self.phitness is None:
            self.phitness = self.fitness()

        if self.age < 0:
            raise ValueError("Negative age")
        if self.weight < 0:
            raise ValueError("Negative weight")

    # O(n)
    def moves(self):
        """
        Calculates probability of an animal deciding to migrate
        and decides if it happens.

        the probability to move is givven by, :math:`\\mu \\Phi`

        :returns: Boolean - True if animal can move.
        """

        p_move = self.default_parameters['mu'] * self.phitness
        return random.random() < p_move

    # O(n)
    def has_moved(self):
        """
        Changes animals movement status to true if animal has moved.
        """

        self.moved = True

    # O(n)
    def reset_moved(self):
        """
        Resets animals movement status to False.
        """
        self.moved = False

    # Finne en måte å kalle mindre på fitness.
    # O(n)
    def ages(self):
        """Animal ages by one year, and re-calculates fitness"""
        self.age += 1
        self.fitness()

    # O(n)
    def birth_weight(self):
        r"""
        Calculates birth weight of an animal.

        The birth weight for a given animal is.

        .. math::

            w \sim \mathcal{N}(w_{birth}, \sigma_{birth})

        :returns: randomized birth weight from gaussian distribution.
        """
        w_birth = self.default_parameters['w_birth']
        sigma_birth = self.default_parameters['sigma_birth']
        weight = random.gauss(w_birth, sigma_birth)
        return weight

    # O(n)
    def gain_weight(self, fodder):
        r"""
        Applies weight gain to Herbivores and re-calculates fitness.

        Given that a herbivore eats an amount :math:`\tilde{F}` of fodder,
        it's weight increases by :math:`\beta \tilde{F}`, where
        :math:`\beta` is a parameter value specific for herbivores

        :param fodder: Amount of fodder to be eaten.
        :returns: updated weight of animal.
        """
        self.weight += self.default_parameters['beta'] * fodder
        self.fitness()
        return self.weight

    def lose_weight(self):
        r"""
        Animal loses weight

        The amount of weight lost equals to :math:`\eta w`

        :returns: updated weight of animal.
        """
        self.weight -= self.default_parameters['eta'] * self.weight
        self.fitness()

    def fitness(self):
        r"""
        Calculates fitness factor of an animal.

        .. math::

            \Phi
            =
            \Biggl \lbrace
            {
            0, {w \leq 0}
            \atop
            q^{+}(a, a_{\frac{1}{2}}, \phi_{age}) \times q^{-}(w, w_{\frac
            {1}{2}}, \phi_{weight}), \text{ else }
            }

        where

        .. math::

            q^{\pm}(x, x_{\frac{1}{2}}, \phi) = \frac{1}{1 + e^{\pm \phi(x- x_{\frac{1}{2}})}}

        Note that :math:`0 \leq \Phi \leq 1`

        :returns: fitness-value of the animal.
        """
        phi_age = self.default_parameters['phi_age']
        a_half = self.default_parameters['a_half']
        phi_weight = self.default_parameters['phi_weight']
        w_half = self.default_parameters['w_half']
        q_plus = (1 / (1 + math.exp(phi_age * (self.age - a_half))))
        q_minus = (1 / (1 + math.exp(-phi_weight * (self.weight - w_half))))
        self.phitness = q_plus * q_minus

        return self.phitness

    def birth(self, n_same_species):
        r"""
        Decides whether an animal gives birth or not
        based on different criterias:

        1: The propability is zero if the weight is

        .. math::

            w < \zeta (w_{birth} + \sigma_{birth})


        where :math:`\zeta, w_{birth}` and :math:`\sigma_{birth}`
        are determined based on species parameters.

        2: Probability for animal getting an offspring:

        .. math::

            min(1, \gamma \times \phi \times (N-1)

        where :math:`N` is the number of animals per species at the start of
        the breeding season, and :math:`\gamma` and :math:`\phi` are
        species-specific parameter values.

        :param n_same_species: Amount of animals in cell.
        :returns: newborn animal if criterias are met.
        :returns: None, if criterias aren't met.
        """
        if self.weight >= self.default_parameters['zeta'] * (
                self.default_parameters['w_birth'] + self.default_parameters['sigma_birth']):
            p_birth = min(
                [1, self.default_parameters['gamma'] * self.phitness * (n_same_species - 1)])

            if random.random() < p_birth:
                weight_birth = self.birth_weight()
                if self.weight - weight_birth > 0:
                    self.weight -= self.default_parameters['xi'] * weight_birth
                    self.fitness()
                    return self.__class__(weight=weight_birth)

    def death(self):
        r"""
        Decides whether an animal dies or not by calculating probability.

        The animal dies with certainty if its weight is :math:`w=0`.

        Otherwise, the probability to die will be given by.

        .. math::
            p_{kill}= \omega(1-\Phi)

        :returns: boolean - True if animal dies.
        """
        if self.weight == 0:
            return True
        else:
            return random.random() < self.default_parameters['omega'] * (1 - self.phitness)


class Herbivores(Animals):
    default_parameters = {'w_birth': 8.0, 'sigma_birth': 1.5,
                          'beta': 0.9, 'eta': 0.05,
                          'a_half': 40.0, 'phi_age': 0.6,
                          'w_half': 10.0, 'phi_weight': 0.1,
                          'mu': 0.25, 'gamma': 0.1,
                          'zeta': 3.5, 'xi': 1.2,
                          'omega': 0.4, 'F': 10.0
                          }

    def __init__(self, age=0, weight=None, fitness=None):
        """
            Subclass of parent class Animal with it's own dictionary of default parameters.

            :param age: Specified age of animal
            :param weight: specified weight of animal

            :raises: ValueError if negative params.
            """
        super().__init__(age, weight, fitness)


class Carnivores(Animals):
    default_parameters = {'w_birth': 6.0, 'sigma_birth': 1.0,
                          'beta': 0.75, 'eta': 0.125,
                          'a_half': 40.0, 'phi_age': 0.3,
                          'w_half': 4.0, 'phi_weight': 0.4,
                          'mu': 0.4, 'gamma': 0.8,
                          'zeta': 3.5, 'xi': 1.1,
                          'omega': 0.8, 'F': 50.0, 'DeltaPhiMax': 10
                          }

    def __init__(self, age=0, weight=None, fitness=None):
        """
            Subclass of parent class Animal with it's own dictionary of default parameters.

            :param age: Specified age of animal
            :param weight: specified weight of animal

            :raises: ValueError if negative params.
            """
        super().__init__(age, weight, fitness)


if __name__ == '__main__':
    pass
