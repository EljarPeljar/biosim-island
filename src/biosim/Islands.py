#!python
# -*- coding: utf-8 -*-
"""
Module containing island class with it's set of attributes.
"""

# finne forbedringspunkter i koden.
from biosim.celle import Water, Lowland, Highland, Desert
import textwrap
import random
import numpy as np

geogr = """\
               WWWWWWWWWWWWWWWWWWWWW
               WWWWWWWWHWWWWLLLLLLLW
               WHHHHHLLLLWWLLLLLLLWW
               WHHHHHHHHHWWLLLLLLWWW
               WHHHHHLLLLLLLLLLLLWWW
               WHHHHHLLLDDLLLHLLLWWW
               WHHLLLLLDDDLLLHHHHWWW
               WWHHHHLLLDDLLLHWWWWWW
               WHHHLLLLLDDLLLLLLLWWW
               WHHHHLLLLDDLLLLWWWWWW
               WWHHHHLLLLLLLLWWWWWWW
               WWWHHHHLLLLLLLWWWWWWW
               WWWWWWWWWWWWWWWWWWWWW
"""


class Island:
    """
    Island class containing cells for every landscape code in multiline string.

    Methods
    -------
    __init__
    island_geography
    create_map
    add_island_pop
    island_feed_all
    island_procreation
    neighbours
    migration
    island_aging
    island_weightloss
    island_death
    number_of_animals
    island_reset_moved
    year_count
    sim_one_year
    """

    def __init__(self, geography):
        """
        Initialize island object and check if the multiline string is compatible.

        :param geography: Multiline string containing landscape code representing each cell.id
        :raises: ValueError if borders isn't strictly water.
        :raises: ValueError if multiline string doesn't have consistent row length.
        :raises: ValueError if landscape code isn't strictly 'W', 'D', 'L' or 'H'
        """
        self.len_map_x = None
        self.len_map_y = None
        self.geogr = geography
        self.island = self.island_geography()
        self.isle = self.create_map()
        self.year = 0

    def island_geography(self):
        """
        Reads through multiline string and check compatibility.

        :return: island_geo, dictionary with key: tuple with coordinates and value: landscape code.
        """

        self.geogr = textwrap.dedent(self.geogr).upper()
        lines = self.geogr.splitlines()
        self.len_map_x = len(lines)
        self.len_map_y = len(lines[0])

        border = lines[0] + lines[-1]
        for line in lines:
            border += line[0] + lines[-1]

        if not set(border) == set('W'):
            raise ValueError("Island must be surrounded by water!")

        island_geo = {}

        for i in range(len(lines) - 1):
            if len(lines[i]) != len(lines[i + 1]):
                raise ValueError("Not consistent row length")

        for i in range(len(lines)):
            for j in range(len(lines[i])):
                if [char for char in lines[i]][j] not in ('W', 'L', 'H', 'D'):
                    raise ValueError(
                        "Geography value not supported: " + [char for char in lines[i]][j])
                else:
                    island_geo[(i + 1, j + 1)] = [char for char in lines[i]][j]

        return island_geo

    def create_map(self):

        """
        Runs through island_geo and creates actual cell objects corresponding to landscape code.

        :return: self.isle, dictionary with key: tuple with coordinates and value: cell objects.
        """

        isle = {}
        for key in self.island:
            if self.island[key] == 'D':
                isle[key] = Desert()
            elif self.island[key] == 'L':
                isle[key] = Lowland()
            elif self.island[key] == 'H':
                isle[key] = Highland()
            else:
                isle[key] = Water()
        self.isle = isle

        return self.isle

    def add_island_pop(self, loc_animal):

        """
        Adds population to given cell with location tuple

        :param loc_animal: list of dictionaries that include keys 'loc' and 'pop'.\
        'loc': is a tuple representing coordinates on the map.keys\
        'pop': is a list of dictionaries which represent each animal that are added.

        :return: self.isle: updated map after adding population to given location.
        """
        for dicti in loc_animal:
            if dicti['loc'] not in self.isle:
                raise KeyError("The desired placement is out of boundary")
            else:
                if dicti['pop'][0]['species'] == 'Herbivore':
                    self.isle[dicti['loc']].add_pop(new_herb=dicti['pop'])

                elif dicti['pop'][0]['species'] == 'Carnivore':
                    self.isle[dicti['loc']].add_pop(new_carn=dicti['pop'])

        return self.isle

    def island_feed_all(self):

        """
        Feeds all animals on the island. Herbivores then carnivores.
        """

        for cell in self.isle:
            self.isle[cell].feed_herbivore()
            self.isle[cell].feed_carnivore()

    def island_procreation(self):
        """
        Determines if animals on the whole island gives birth or not.
        Updates relevant cells if babies are born.
        """
        for cell in self.isle:
            self.isle[cell].annual_birth()

    def neighbours(self, cell):

        """
        Computes the cardinal neighbouring cells for a given cell.

        :param cell: tuple representing coordinates for a cell.

        :returns: list of tuples, each representing their respective cell position.
        """

        n_coord, e_coord = cell

        loc_e = (n_coord, e_coord + 1)  # cell to the East
        loc_w = (n_coord, e_coord - 1)  # cell to the West
        loc_s = (n_coord + 1, e_coord)  # cell to the South
        loc_n = (n_coord - 1, e_coord)  # cell to the North

        loc_list = [loc_e, loc_w, loc_n, loc_s]

        loc_list = [elem for elem in loc_list if elem in self.isle]

        return loc_list

    def migration(self):
        """
        Determines if animals move to a neighbouring cell or not.
        """
        for coord in self.isle:
            neighbour = self.neighbours(coord)
            moving_herbs, moving_carns = self.isle[coord].animals_moving()
            for h in moving_herbs:
                choice = random.choice(neighbour)
                if self.isle[choice].landscape != 'W' and h.moved is False:
                    h.has_moved()
                    self.isle[choice].herbs.append(h)
                else:
                    self.isle[coord].herbs.append(h)

            for c in moving_carns:
                choice = random.choice(neighbour)
                if self.isle[choice].landscape != 'W' and c.moved is False:
                    c.has_moved()
                    self.isle[choice].carns.append(c)
                else:
                    self.isle[coord].carns.append(c)

    def island_aging(self):  # repeat island?
        """
        Makes every animal on the island age by +1.
        """
        for cell in self.isle:
            self.isle[cell].annual_aging()

    def island_weightloss(self):
        """
        Makes every animal on the island lose weight by natural yearly cause.
        """
        for cell in self.isle:
            self.isle[cell].annual_weight_loss()

    def island_deaths(self):
        """
        Runs natural deaths in each cell for each species.
        """

        for cell in self.isle:
            self.isle[cell].annual_death()

    def one_cell_count(self):
        check = np.zeros([self.len_map_x, self.len_map_y])
        check_h = np.zeros([self.len_map_x, self.len_map_y])
        check_c = np.zeros([self.len_map_x, self.len_map_y])
        for _coord in self.isle:
            count_cell = self.isle[_coord].animal_count()[2]
            count_cell_h = self.isle[_coord].animal_count()[0]
            count_cell_c = self.isle[_coord].animal_count()[1]
            check[_coord[0] - 1, _coord[1] - 1] = count_cell
            check_h[_coord[0] - 1, _coord[1] - 1] = count_cell_h
            check_c[_coord[0] - 1, _coord[1] - 1] = count_cell_c
        return check, check_h, check_c

    def number_of_animals(self):

        """
        Sums up the population for each species.

        :return: list containing total population of herbivores, carnivores and total of both.
        """

        herbivore_count_year = 0
        carnivore_count_year = 0

        for _coord in self.isle:
            herbivore_count_year += self.isle[_coord].animal_count()[0]
            carnivore_count_year += self.isle[_coord].animal_count()[1]

        total_count_year = herbivore_count_year + carnivore_count_year
        return [herbivore_count_year, carnivore_count_year, total_count_year]

    def island_reset_moved(self):
        """
        Resets values of cells and animals that needs to be reset before the new year.
        """

        for cells in self.isle:
            for animals in self.isle[cells].herbs + self.isle[cells].carns:
                animals.reset_moved()

    def year_count(self):
        """
        Adds +1 to the year each time its called.
        """

        self.year += 1
        return self.year

    def sim_one_year(self):
        """
        Simulates one year in a specific order given in project description.

        :return: poplation of animals by species and total.
        """
        self.island_reset_moved()
        self.island_procreation()
        self.island_feed_all()
        self.migration()
        self.island_aging()
        self.island_weightloss()
        self.island_deaths()
        self.number_of_animals()
        self.year_count()

        return self.number_of_animals()
