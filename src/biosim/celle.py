from biosim.Fauna import Herbivores, Carnivores
import random


# noinspection PyTypeChecker
class Cell:
    """
    Parent class for all cell types (Water, Desert, Lowland and Highland)

    Methods
    -------
    cell set_params: class method
    __init__
    check_access
    add_pop
    animals_moving
    annual_aging
    animal_count
    feed_herbivore
    feed_carnivore
    annual_weight_loss
    annual_birth
    annual_death
    """

    default_params_cell = {'f_max': 900}
    landscape = None
    accessible = True

    @classmethod
    def cell_set_params(cls, new_params):
        """
        Updates parameters if needed

        :param new_params: dictionary of new parameters to be updated.
        :raises: ValueError if criterias aren't met.
        """
        for key in new_params:
            if key != 'f_max':
                raise KeyError('Invalid parameter name: ' + key)

        for params in new_params:
            if not 0 <= new_params[params]:
                raise ValueError(params, ": Must be larger or equal to zero")

            cls.default_params_cell.update(new_params)

    def __init__(self, herbs=None, carns=None):
        """
        Initializes a cell object and checks the corresponding
        population. Can add population to the cell if told to.

        :param herbs: list of dictionaries specifying age and weight of the herbivore
        :param carns: list of dictionaries specifying age and weight of the carnivore
        """

        self.herbs = herbs
        self.carns = carns

        if self.carns is None:
            self.carns = []
        else:
            self.check_access()
            self.carns = [Carnivores(element['age'], element['weight'])
                          for element in carns if element['species'] == 'Carnivore']

        if self.herbs is None:
            self.herbs = []
        else:
            self.check_access()
            self.herbs = [Herbivores(element['age'], element['weight'])
                          for element in herbs if element['species'] == 'Herbivore']

        # self.fodder = None

    def add_pop(self, new_herb=None, new_carn=None):
        """
        Adds population to the cell either herbivores or carnivores.

        :param new_herb: list of dictionaries for each herbivore to be added.
        :param new_carn: list of dictionaries for each carnivore to be added.

        :return: updated lists of herbivores and carnivores residing in the cell.
        """
        if self.landscape != 'W':
            if new_herb is not None:
                for element in new_herb:
                    if element['species'] == 'Herbivore':
                        herbivore = Herbivores(element['age'], element['weight'])
                        self.herbs.append(herbivore)
            # return self.herbs

            if new_carn is not None:
                for element in new_carn:
                    if element['species'] == 'Carnivore':
                        carnivore = Carnivores(element['age'], element['weight'])
                        self.carns.append(carnivore)
            # return self.carns
        else:
            self.check_access()

        return self.herbs, self.carns

    def animals_moving(
            self):  # possible problem here as we iterate through same list that we delete from.
        """
        Appends animals to a list if they are moving.

        :return: list of herbivores moving and list of carnivores moving.
        """

        moving_animals_herb = []
        moving_animals_carn = []
        for herbivore in self.herbs:
            if herbivore.moves():
                moving_animals_herb.append(herbivore)

        self.herbs = [herb for herb in self.herbs if herb not in moving_animals_herb]

        for carnivore in self.carns:
            if carnivore.moves():
                moving_animals_carn.append(carnivore)

        self.carns = [carn for carn in self.carns if carn not in moving_animals_carn]

        return moving_animals_herb, moving_animals_carn

    def annual_aging(self):
        """
        Adds +1 to all animals in the current cells
        """
        for ani in self.herbs + self.carns:
            ani.ages()

    def animal_count(self):
        """
        Determines the number of animals per species in a cell.

        :return: list of integers specifying the population of herbivores, carnivores and total
        population-
        """
        herb_count = len(self.herbs)
        carn_count = len(self.carns)
        total_count = herb_count + carn_count

        return [herb_count, carn_count, total_count]

    def feed_herbivore(self):
        """
        Feeds every herbivore in the current cell.
        """

        fodder = self.default_params_cell['f_max']
        random.shuffle(self.herbs)

        for herbivore in self.herbs:
            if fodder >= herbivore.default_parameters['F']:
                herbivore.gain_weight(herbivore.default_parameters['F'])
                fodder -= herbivore.default_parameters['F']
            elif 0 < fodder < herbivore.default_parameters['F']:
                herbivore.gain_weight(fodder)
                fodder = fodder - fodder

        # self.fodder = fodder

    def feed_carnivore(self):

        # Liste kjøretid  - reversed - bucketsort???
        # forbedre feeding koden
        self.herbs = sorted(self.herbs, key=lambda x: x.phitness)
        self.carns = sorted(self.carns, key=lambda x: x.phitness, reverse=True)

        for carn in self.carns:
            appetite = 0
            killed_herbs = []
            if len(self.herbs) == 0:
                break
            for herb in self.herbs:
                if carn.phitness <= herb.phitness:
                    continue
                elif 0 < (carn.phitness - herb.phitness) < carn.default_parameters['DeltaPhiMax']:
                    p_kill = (carn.phitness - herb.phitness) / (
                        carn.default_parameters['DeltaPhiMax'])
                else:
                    p_kill = 1
                if appetite <= carn.default_parameters['F'] and random.random() < p_kill:
                    if appetite + herb.weight > carn.default_parameters['F']:
                        weight_remaining = carn.default_parameters['F'] - appetite
                        appetite += weight_remaining
                        carn.weight += carn.default_parameters['beta'] * weight_remaining
                        killed_herbs.append(herb)
                        carn.fitness()
                        break
                    else:
                        carn.weight += carn.default_parameters['beta'] * herb.weight
                        appetite += herb.weight
                        killed_herbs.append(herb)
                        carn.fitness()

            self.herbs = [herb for herb in self.herbs if herb not in killed_herbs]

    def annual_weight_loss(self):
        """
        Makes every animal in the cell lose weight caused by yearly weightloss.
        """
        for animal in self.herbs + self.carns:
            animal.lose_weight()

    def annual_birth(self):  # Kan endre på struktur - unngå bruk av lister.
        """
        Determines if any of the animals in the current cell gives birth.
        Updates list of species if a baby is born.
        """
        newborn_herb_list = []
        newborn_carn_list = []

        for herb in self.herbs:
            newborn_herb = herb.birth(len(self.herbs))
            if newborn_herb is not None:
                newborn_herb_list.append(newborn_herb)

        self.herbs.extend(newborn_herb_list)

        for carn in self.carns:
            newborn_carn = carn.birth(len(self.carns))
            if newborn_carn is not None:
                newborn_carn_list.append(newborn_carn)

        self.carns.extend(newborn_carn_list)

    def annual_death(self):
        """
        Determines if any of the animals in the cells dies of natural causes.
        Updates the list after animals die.
        """

        for herb in self.herbs[:]:
            if herb.death():
                self.herbs.remove(herb)

        for carn in self.carns[:]:
            if carn.death():
                self.carns.remove(carn)


class Water(Cell):
    """
    Subclass of parent class Cell with it's own dictionary of default parameters.

    :raises: KeyError if attempting to add animals to Water cell.
    """
    landscape = 'W'
    default_params_cell = {'f_max': 0}
    accessible = False


class Desert(Cell):
    """
    Subclass of parent class Cell with it's own dictionary of default parameters.
    """
    landscape = 'D'
    default_params_cell = {'f_max': 0}


class Lowland(Cell):
    """
    Subclass of parent class Cell with it's own dictionary of default parameters.
    """
    landscape = 'L'
    default_params_cell = {'f_max': 800}


class Highland(Cell):
    """
    Subclass of parent class Cell with it's own dictionary of default parameters.
    """
    landscape = 'H'
    default_params_cell = {'f_max': 300}


if __name__ == '__main__':
    pass

added_pop = [{'species': 'Herbivore', 'age': 3, 'weight': 10.5} for _ in range(4)]
added_popp = [{'species': 'Carnivore', 'age': 2, 'weight': 30.5} for _ in range(4)]

# Look like feeding herbivores i going godd, but I cant feed Carnivores(not updating fitness)
