"""
Template for BioSim class.
"""

from biosim.celle import Water, Desert, Highland, Lowland
from biosim.Fauna import Herbivores, Carnivores
from biosim.Islands import Island
from biosim.visualizattion import Visualize
import matplotlib.pyplot as plt
import random

class BioSim:
    """
    Top-level interface to BioSim package.
    """

    def __init__(self, island_map, ini_pop, seed,
                 vis_years=1, ymax_animals=None, cmax_animals=None, hist_specs=None,
                 img_years=None, img_dir=None, img_base=None, img_fmt='png',
                 log_file=None):

        """
        Parameters
        ----------
        island_map : str
            Multi-line string specifying island geography
        ini_pop : list
            List of dictionaries specifying initial population
        seed : int
            Integer used as random number seed
        vis_years : int
            Years between visualization updates (if 0, disable graphics)
        ymax_animals : int
            Number specifying y-axis limit for graph showing animal numbers
        cmax_animals : dict
            Color-scale limits for animal densities, see below
        hist_specs : dict
            Specifications for histograms, see below
        img_years : int
            Years between visualizations saved to files (default: `vis_years`)
        img_dir : str
            Path to directory for figures
        img_base : str
            Beginning of file name for figures
        img_fmt : str
            File type for figures, e.g. 'png' or 'pdf'
        log_file : str
            If given, write animal counts to this file

        Notes
        -----
        - If `ymax_animals` is None, the y-axis limit should be adjusted automatically.
        - If `cmax_animals` is None, sensible, fixed default values should be used.
        - `cmax_animals` is a dict mapping species names to numbers, e.g.,

          .. code:: python

             {'Herbivore': 50, 'Carnivore': 20}

        - `hist_specs` is a dictionary with one entry per property for which a histogram
          shall be shown. For each property, a dictionary providing the maximum value
          and the bin width must be given, e.g.,

          .. code:: python

             {'weight': {'max': 80, 'delta': 2},
              'fitness': {'max': 1.0, 'delta': 0.05}}

          Permitted properties are 'weight', 'age', 'fitness'.
        - If `img_dir` is None, no figures are written to file.
        - Filenames are formed as

          .. code:: python

             Path(img_dir) / f'{img_base}_{img_number:05d}.{img_fmt}'

          where `img_number` are consecutive image numbers starting from 0.

        - `img_dir` and `img_base` must either be both None or both strings.
        """
        self.island_map = island_map
        random.seed(seed)

        self.ymax_animals = ymax_animals
        self.cmax_animals = cmax_animals
        self.hist_specs = hist_specs
        self.isl = Island(island_map)
        self.graphx = Visualize(self.isl)
        self.isl.add_island_pop(ini_pop)
        self.initial_pop = ini_pop
        self.length_x = self.isl.len_map_x
        self.length_y = self.isl.len_map_y

    def set_animal_parameters(self, species, params):
        """
        Set parameters for animal species.

        :param species: String, name of animal species
        :param params: Dict with valid parameter specification for species
        """
        animal_species = {'Herbivore': Herbivores,
                          'Carnivore': Carnivores}

        animal_species[species].set_params(params)

    @staticmethod
    def set_landscape_parameters(geo, params):
        """
        Set parameters for landscape type.

        :param geo: String, code letter for landscape
        :param params: Dict with valid parameter specification for landscape
        """
        geo_code = {'W': Water,
                    'D': Desert,
                    'L': Lowland,
                    'H': Highland}

        geo_code[geo].cell_set_params(params)

    def simulate(self, num_years):
        """
        Run simulation while visualizing the result.

        Parameters
        ----------
        num_years : int
            Number of years to simulate
        """

        # Det jeg trenger her er en for loop. Setter opp kartet - hele plot
        # Trenger å telle antall dyr som blir skrevet ut terminal.
        # Trenger grafene. Så oppdaterer jeg grafene.
        # simulerer for ett visst antall år.
        # lager visualization først.
        self.graphx.setup(num_years, 1)
        if self.graphx.colored_map(self.island_map) is None:
            self.graphx.colored_map(self.island_map)
        else:
            pass

        for _ in range(num_years):
            print("Herbivores: {}, Carnivores: {}, Total: {}, Year: {}"
                  .format(self.isl.number_of_animals()[0], self.isl.number_of_animals()[1],
                          self.isl.number_of_animals()[2], self.isl.year))
            self.graphx.population_graph(self.total_num_herbivores(),
                                         self.total_num_carnivores(),
                                         self.isl.year)
            self.graphx.update_graphics(
                self.get_population()[0], self.get_population()[1])
            herb_fit, carn_fit = self.get_fitness()
            herb_age, carn_age = self.get_age()
            herb_weight, carn_weight = self.get_weight()
            self.graphx.histogram_fitness(herb_fit, carn_fit)
            self.graphx.histogram_age(herb_age, carn_age)
            self.graphx.histogram_weight(herb_weight, carn_weight)
            self.graphx.update_yearcount()
            self.graphx._save_graphics(num_years)
            plt.pause(10e-3)
            self.isl.sim_one_year()

    def add_population(self, population):
        """
        Add a population to the island

        Parameters
        ----------
        population : List of dictionaries
            See BioSim Task Description, Sec 3.3.3 for details.
        """
        self.isl.add_island_pop(population)

    @property
    def year(self):
        """Last year simulated."""
        return self.isl.year

    @property
    def num_animals(self):
        """Total number of animals on island."""
        return self.isl.number_of_animals()[2]

    @property
    def num_animals_per_species(self):
        """Number of animals per species in island, as dictionary."""

        return {'Herbivore': self.isl.number_of_animals()[0],
                'Carnivore': self.isl.number_of_animals()[1]}

    def total_num_herbivores(self):
        """
        Returns the total number of herbivores on the island.
        :return: Integer of total number of herbivores
        """
        total_herbivores = self.isl.number_of_animals()[0]
        return total_herbivores

    def total_num_carnivores(self):
        """
        Returns the total number of carnivores on the island.
        :return: Integer of total number of carnivores
        """
        total_carnivores = self.isl.number_of_animals()[1]
        return total_carnivores

    def get_population(self):
        """
        Counts animal population by species in each cell.

        :return: Nested list for each species with animal count in same position as coord.
        """
        nested_list_herb = []
        nested_list_carn = []

        for x in range(self.length_x):
            nested_list_herb.append([])
            nested_list_carn.append([])
            for y in range(self.length_y):
                nested_list_herb[x].append(
                    self.isl.isle[(x + 1, y + 1)].animal_count()[0])
                nested_list_carn[x].append(
                    self.isl.isle[(x + 1, y + 1)].animal_count()[1])

        return nested_list_herb, nested_list_carn

    def get_fitness(self):
        """
        Lists up all fitness values for all animals by species in to separate lists.

        :return: List containing fitness values for all animals.
        """
        herb_fitness = []
        carn_fitness = []
        for coord in self.isl.isle:
            cell = self.isl.isle[coord]
            for herb in cell.herbs:
                herb_fitness.append(herb.phitness)
            for carn in cell.carns:
                carn_fitness.append(carn.phitness)

        return herb_fitness, carn_fitness

    def get_age(self):
        """
        Lists up age of all animals by species in to separate lists.

        :return: List containing age values for all animals.
        """

        herb_age = []
        carn_age = []
        for coord in self.isl.isle:
            cell = self.isl.isle[coord]
            for herb in cell.herbs:
                herb_age.append(herb.age)
            for carn in cell.carns:
                carn_age.append(carn.age)
        return herb_age, carn_age

    def get_weight(self):
        """
        Lists up weight of all animals by species in to separate lists.

        :return: List containing weight values for all animals.
        """

        herb_weight = []
        carn_weight = []
        for coord in self.isl.isle:
            cell = self.isl.isle[coord]
            for herb in cell.herbs:
                herb_weight.append(herb.weight)
            for carn in cell.carns:
                carn_weight.append(carn.weight)
        return herb_weight, carn_weight

    def make_movie(self, movie_fmt=None):
        """Create MPEG4 movie from visualization images saved."""
        self.graphx.make_movie(movie_fmt)
