#!python
# -*- coding: utf-8 -*-
"""
Module containing visualization class with it's set of attributes.
"""

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import os
import textwrap

_FFMPEG_BINARY = 'ffmpeg'
_MAGICK_BINARY = 'magick'

# update this to the directory and file-name beginning
# for the graphics files
_DEFAULT_GRAPHICS_DIR = os.path.join('..', 'data')
_DEFAULT_GRAPHICS_NAME = 'biosim'
_DEFAULT_IMG_FORMAT = 'png'
_DEFAULT_MOVIE_FORMAT = 'mp4'  # alternatives: mp4, gif


class Visualize:
    """
    Visualizes the simulation with updated statistics.

    Methods
    -------
    __init__
    update_graphics
    update_yearcount
    make_movie
    setup
    _update_system_map
    _save_graphics
    colored_map
    heat_map_herbs
    heat_map_carns
    population_graph
    histogram_fitness
    histogram_age
    histogram_weight
    """

    def __init__(self, island, img_dir=None, img_name=None, img_fmt=None):
        """
        Initialize visualization class and sets default parameters unless otherwise specified.

        :param island: Island instance from BioSim.
        :param img_dir: Directory where movie is to be saved
        :param img_name: Name of movie
        :param img_fmt: Format of movie
        """
        self.graf_island = island
        if img_name is None:
            img_name = _DEFAULT_GRAPHICS_NAME

        if img_dir is None:
            img_dir = _DEFAULT_GRAPHICS_DIR

        if img_dir is not None:
            self._img_base = os.path.join(img_dir, img_name)
        else:
            self._img_base = None

        self._img_fmt = img_fmt if img_fmt is not None else _DEFAULT_IMG_FORMAT

        self._img_ctr = 0
        self._img_step = 1

        self.all_herbs = []
        self.all_carns = []
        self.all_years = []

        # the following will be initialized by _setup_graphics
        self._fig = None
        self.grid = None

        self._map_fig = None
        self._map_axis = None

        self._year_count = None

        self._pop_fig = None
        self._pop_herb = None
        self._pop_carn = None
        self._pop_x_axis = None
        self._pop_y_axis = None

        self._heatmap_herbs_fig = None
        self._heatmap_herbs_axis = None
        self.axlg_heat_herbs = None

        self._heatmap_carns_fig = None
        self._heatmap_carns_axis = None
        self.axlg_heat_carns = None

        self._fit_fig = None
        self._fit_herb = None
        self._fit_carn = None

        self._age_fig = None
        self._age_herb = None
        self._age_carn = None

        self._weight_fig = None
        self._weight_herb = None
        self._weight_carn = None

    def update_graphics(self, nested_list_herbs, nested_list_carn):
        """
        Upddates heatmaps for herbivores and carnivores.
        """

        self.heat_map_herbs(nested_list_herbs)
        self.heat_map_carns(nested_list_carn)

    def update_yearcount(self):
        """
        Updates the year counter on the graphics.
        """

        self._year_count = self._fig.suptitle("YEAR COUNT: " + str(self.graf_island.year))

    def colored_map(self, island_string):
        plot_geo = textwrap.dedent(island_string)

        rgb_value = {'W': (0.0, 0.0, 1.0),  # blue
                     'L': (0.0, 0.6, 0.0),  # dark green
                     'H': (0.5, 1.0, 0.5),  # light green
                     'D': (1.0, 1.0, 0.5)}  # light yellow

        map_rgb = [[rgb_value[column] for column in row]
                   for row in plot_geo.splitlines()]
        self._map_axis = self._map_fig.imshow(map_rgb)

        self._map_fig.set_xticks(range(len(map_rgb[0])))

        labels_x = [i + 1 if (i == 0 or i % 5 == 4)
                    else None for i in range(len(map_rgb[0]))]
        self._map_fig.set_xticklabels(labels_x)
        self._map_fig.set_yticks(range(len(map_rgb)))
        labels_y = [i + 1 if (i == 0 or i % 5 == 4)
                    else None for i in range(len(map_rgb))]

        self._map_fig.set_yticklabels(labels_y)

        axlg_map = self._fig.gca()  # llx, lly, w, h,[0.32, 0.75, 0.1, 0.3]
        axlg_map.axis('off')

    def make_movie(self, movie_fmt=None):
        """
        Creates MPEG4 movie from visualization images saved.

        .. :note:
            Requires ffmpeg for MP4 and magick for GIF

        The movie is stored as img_base + movie_fmt
        :param movie_fmt: Format of movie
        """

        if self._img_base is None:
            raise RuntimeError("No filename defined.")

        if movie_fmt is None:
            movie_fmt = _DEFAULT_MOVIE_FORMAT

        if movie_fmt == 'mp4':
            try:
                # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
                # section "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY,
                                       '-i', '{}_%05d.png'.format(self._img_base),
                                       '-y',
                                       '-profile:v', 'baseline',
                                       '-level', '3.0',
                                       '-pix_fmt', 'yuv420p',
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))
        elif movie_fmt == 'gif':
            try:
                subprocess.check_call([_MAGICK_BINARY,
                                       '-delay', '1',
                                       '-loop', '0',
                                       '{}_*.png'.format(self._img_base),
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: convert failed with: {}'.format(err))
        else:
            raise ValueError('Unknown movie format: ' + movie_fmt)

    def setup(self, final_step, img_step):
        """
        Prepare graphics.

        Call this before calling :meth:`update()` for the first time after
        the final time step has changed.

        :param final_step: last time step to be visualised (upper limit of x-axis)
        :param img_step: interval between saving image to file
        """

        self._img_step = img_step

        # create new figure window
        if self._fig is None:
            self._fig = plt.figure(constrained_layout=True, figsize=(15, 10))
            self.grid = self._fig.add_gridspec(10, 24)

        # Add left subplot for images created with imshow().
        # We cannot create the actual ImageAxis object before we know
        # the size of the image, so we delay its creation.
        if self._map_fig is None:
            self._map_fig = self._fig.add_subplot(self.grid[0:3, :8])
            self._map_fig.set_title("Island")
            self._map_axis = None

        if self._pop_fig is None:
            self._pop_fig = self._fig.add_subplot(self.grid[0:3, -8:])
            self._pop_fig.set_xlim([0, final_step + 100])
            self._pop_x_axis = None
            self._pop_y_axis = None
            self._pop_fig.set_title("Animal count")
            self._pop_fig.set_ylabel("NO. OF ANIMALS")
            self._pop_fig.set_xlabel("YEARS SIMULATED")

        #
        if self._heatmap_herbs_fig is None:
            self._heatmap_herbs_fig = self._fig.add_subplot(self.grid[4:7, :9])
            self._heatmap_herbs_axis = None
            self.axlg_heat_herbs = None
            self._heatmap_herbs_fig.set_title("Herbivore distribution")

        if self._heatmap_carns_fig is None:
            self._heatmap_carns_fig = self._fig.add_subplot(self.grid[4:7, -9:])
            self._heatmap_carns_axis = None
            self.axlg_heat_carns = None
            self._heatmap_carns_fig.set_title("Carnivore distribution")
        #
        if self._fit_fig is None:
            self._fit_fig = self._fig.add_subplot(self.grid[8:10, :7])
            self._fit_herb = None
            self._fit_carn = None

            self._fit_fig.set_xlim([0, 1])
            self._fit_fig.set_title("Fitness histogram")

        #
        if self._age_fig is None:
            self._age_fig = self._fig.add_subplot(self.grid[8:10, 9:16])
            self._age_herb = None
            self._age_carn = None
            self._age_fig.set_title("Age histogram")

        if self._weight_fig is None:
            self._weight_fig = self._fig.add_subplot(self.grid[8:10, 18:25])
            self._weight_herb = None
            self._weight_carn = None
            self._weight_fig.set_title("Weight histogram")

    def _update_system_map(self, sys_map):
        """
        Update the 2D-view of the system.
        :param sys_map:
        """

        if self._map_axis is not None:
            self._map_axis.set_data(sys_map)
        else:
            self._map_axis = self._map_fig.imshow(sys_map,
                                                  interpolation='nearest',
                                                  vmin=-0.25, vmax=0.25)
            plt.colorbar(self._map_axis, ax=self._map_axis,
                         orientation='horizontal')

    def _save_graphics(self, step):
        """
        Saves graphics to file if file name given.
        :param step: Current time step
        """

        if self._img_base is None or step % self._img_step != 0:
            return

        plt.savefig('{base}_{num:05d}.{type}'.format(base=self._img_base,
                                                     num=self._img_ctr,
                                                     type=self._img_fmt))
        self._img_ctr += 1

    def heat_map_herbs(self, nested_list):
        """
        Makes a 'heat map', with more intense coloration the more herbivores that lives there
        :param nested_list: A nested list with the amount of herbivores in each cell
        """

        self._heatmap_herbs_fig.set_xticks(range(len(nested_list[0])))
        labels_x = [i + 1 if (i == 0 or i % 5 == 4) else None for i in range(len(nested_list[0]))]
        self._heatmap_herbs_fig.set_xticklabels(labels_x)
        self._heatmap_herbs_fig.set_yticks(range(len(nested_list)))
        labels_y = [i + 1 if (i == 0 or i % 5 == 4) else None for i in range(len(nested_list))]
        self._heatmap_herbs_fig.set_yticklabels(labels_y)

        if self.axlg_heat_herbs is None:
            self.axlg_heat_herbs = self._fig.add_axes([0.85, 0.1, 0.1, 0.8])  # llx, lly, w, h
            self.axlg_heat_herbs.axis('off')

        if self._heatmap_herbs_axis is not None:
            self._heatmap_herbs_axis.set_data(np.array(nested_list))
        else:
            self._heatmap_herbs_axis = self._heatmap_herbs_fig.imshow(nested_list, vmax=200,
                                                                      interpolation='nearest',
                                                                      cmap='viridis')

            self.axlg_heat_herbs.figure.colorbar(self._heatmap_herbs_axis,
                                                 ax=self._heatmap_herbs_fig, orientation='vertical',
                                                 fraction=0.07, pad=0.04)

    def heat_map_carns(self, nested_list):
        """
        Makes a 'heat map', with intense coloration the more carnivores that lives there
        :param nested_list: A nested list with the amount of carnivores in each cell
        """

        self._heatmap_carns_fig.set_xticks(range(len(nested_list[0])))
        labels_x = [i + 1 if (i == 0 or i % 5 == 4) else None for i in range(len(nested_list[0]))]
        self._heatmap_carns_fig.set_xticklabels(labels_x)
        self._heatmap_carns_fig.set_yticks(range(len(nested_list)))
        labels_y = [i + 1 if (i == 0 or i % 5 == 4) else None for i in range(len(nested_list))]
        self._heatmap_carns_fig.set_yticklabels(labels_y)

        if self.axlg_heat_carns is None:
            self.axlg_heat_carns = self._fig.gca()
            self.axlg_heat_carns.axis('off')

        if self._heatmap_carns_axis is not None:
            self._heatmap_carns_axis.set_data(np.array(nested_list))
        else:
            self._heatmap_carns_axis = self._heatmap_carns_fig.imshow(nested_list, vmax=200,
                                                                      interpolation='nearest',
                                                                      cmap='inferno')

            self.axlg_heat_carns.figure.colorbar(self._heatmap_carns_axis,
                                                 ax=self._heatmap_carns_fig, orientation='vertical',
                                                 fraction=0.07, pad=0.04)

    def population_graph(self, num_herb, num_carn, year):
        """
        Makes line-graphs of the population by species over time.

        :param num_herb: Integer representing the number of herbivores on the island for current year.
        :param num_carn: Integer representing the number of carnivores on the island for current year.
        :param year: Integer representing the current year after simulation started.
        """

        self.all_herbs.append(num_herb)
        self.all_carns.append(num_carn)
        self.all_years.append(year)

        self._pop_herb = self._pop_fig.plot(self.all_years, self.all_herbs, color='g', label='Herbivore')
        self._pop_herb = self._pop_fig.plot(self.all_years, self.all_carns, color='r', label='Carnivore')

    def histogram_fitness(self, herb_fit, carn_fit):
        """
        Makes histograms of the fitness levels of herbivores and carnivores.
        Calls upon .cla() each time so the histogram gets wiped.

        :param herb_fit: List containing fitness levels of all herbivores in a given year.
        :param carn_fit: List containing fitness levels of all carnivores in a given year.
        """

        self._fit_fig.cla()
        self._fit_fig.set_title("Fitness histogram")
        self._fit_fig.set_ylabel("NO. OF ANIMALS", fontsize=10)
        self._fit_fig.set_xlabel("FITNESS LEVELS", fontsize=10)

        self._fit_fig.set_xlim([0, 1])

        self._fit_herb = self._fit_fig.hist(herb_fit, histtype='step', color='g', label='Herbivore', bins=25)
        self._fit_carn = self._fit_fig.hist(carn_fit, histtype='step', color='r', label='Herbivore', bins=25)

    def histogram_age(self, herb_age, carn_age):
        """
        Makes histograms of herbivore and carnivore ages.
        Calls upon .cla() each time so the histogram gets wiped.

        :param herb_age: List containing age of all herbivores in a given year.
        :param carn_age: List containing age of all carnivores in a given year.
        """

        self._age_fig.cla()
        self._age_fig.set_title("Age histogram")
        self._age_fig.set_ylabel("NO. OF ANIMALS", fontsize=10)
        self._age_fig.set_xlabel("AGE", fontsize=10)
        self._age_fig.set_xlim([0, 60])

        self._age_herb = self._age_fig.hist(herb_age, histtype='step', color='g', label='Herbivore', bins=25)
        self._age_carn = self._age_fig.hist(carn_age, histtype='step', color='r', label='Herbivore', bins=25)

    def histogram_weight(self, herb_weight, carn_weight):
        """
       Makes histograms of herbivore and carnivore weights.
       Calls upon .cla() each time so the histogram gets wiped.

       :param herb_weight: List containing weight of all herbivores in a given year.
       :param carn_weight: List containing weight of all carnivores in a given year.
       """

        self._weight_fig.cla()
        self._weight_fig.set_title("Weight histogram")
        self._weight_fig.set_ylabel("NO. OF ANIMALS", fontsize=10)
        self._weight_fig.set_xlabel("WEIGHT", fontsize=10)
        self._weight_fig.set_xlim([0, 80])

        self._weight_herb = self._weight_fig.hist(herb_weight, histtype='step', color='g', label="Herbivore", bins=25)
        self._weight_carn = self._weight_fig.hist(carn_weight, histtype='step', color='r', label='Herbivore', bins=25)


