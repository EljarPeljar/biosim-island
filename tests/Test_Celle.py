import biosim.celle as celle
import biosim.Fauna as Fauna
import pytest

c1 = celle.Lowland()
c2 = celle.Highland()
c3 = celle.Desert()
c4 = celle.Water()
c5 = celle.Lowland()
c6 = celle.Highland()
c7 = celle.Desert()
c8 = celle.Lowland()
c9 = celle.Highland()
c10 = celle.Desert()
c11 = celle.Lowland()
c12 = celle.Highland()
c13 = celle.Desert()


def test_params_L():
    "tests if cell parameters are correct"
    p = {'f_max': 800}
    assert c1.default_params_cell == p and c3.accessible == True


def test_params_H():
    "tests if cell parameters are correct"
    p = {'f_max': 300}
    assert c2.default_params_cell == p and c3.accessible == True


def test_params_D():
    "tests if cell parameters are correct"
    p = {'f_max': 0}
    assert c3.default_params_cell == p and c3.accessible == True


def test_params_W():
    "tests if cell parameters are correct"
    p = {'f_max': 0}
    assert c4.default_params_cell == p and c4.accessible == False


@pytest.mark.parametrize("Cells", [c5, c6, c7])
def test_add_pop(Cells):
    "tests if the add_pop function adds herbs to the cell as intended"
    added_pop = [{'species': 'Herbivore', 'age': 3, 'weight': 8} for _ in range(2)]
    Cells.add_pop(added_pop)
    assert Cells.animal_count()[2] == 2


def test_add_pop_w():
    h1 = {'species': 'Herbivore', 'age': 3, 'weight': 8}
    with pytest.raises(AttributeError):
        c4.add_pop(new_herb=[h1])


@pytest.mark.parametrize("Cells", [c1, c2, c3])
def test_animals_moving(Cells):
    """Check if the animals moves"""
    "Use lambda to decide weather the -moves- gives the outcome we need so we can test the function"
    h1 = Fauna.Herbivores()
    h2 = Fauna.Herbivores()

    ca1 = Fauna.Carnivores()
    ca2 = Fauna.Carnivores()

    Cells.herbs = [h1, h2]
    Cells.carns = [ca1, ca2]

    h1.moves = lambda: False
    h2.moves = lambda: False
    ca2.moves = lambda: False
    ca1.moves = lambda: False

    herb_mov, carn_mov = Cells.animals_moving()

    assert len(herb_mov) == 0
    assert len(carn_mov) == 0

    h1.moves = lambda: True
    h2.moves = lambda: False
    ca2.moves = lambda: True
    ca1.moves = lambda: False

    herb_mov, carn_mov = Cells.animals_moving()

    assert len(herb_mov) == 1
    assert len(carn_mov) == 1


@pytest.mark.parametrize("Cells", [c11, c12, c13])
def test_annual_aging(Cells):
    h1 = {'species': 'Herbivore', 'age': 3, 'weight': 8}
    c1 = {'species': 'Carnivore', 'age': 3, 'weight': 8}
    Cells.add_pop(new_herb=[h1])
    Cells.add_pop(new_carn=[c1])
    assert Cells.herbs[0].age == 3
    assert Cells.carns[0].age == 3
    Cells.annual_aging()
    assert Cells.herbs[0].age == 4
    assert Cells.herbs[0].age == 4


@pytest.mark.parametrize("Cells", [c8, c9, c10])
def test_animal_count(Cells):
    Cells.add_pop(new_herb=[{'species': 'Herbivore', 'age': 3, 'weight': 8} for _ in range(7)])
    Cells.add_pop(new_carn=[{'species': 'Carnivore', 'age': 3, 'weight': 8} for _ in range(4)])
    assert Cells.animal_count()[0] == 7
    assert Cells.animal_count()[1] == 4
    assert Cells.animal_count()[2] == 11


@pytest.mark.parametrize("Cells", [c1, c2])
def test_feed_herbivore(Cells):
    h1 = {'species': 'Herbivore', 'age': 3, 'weight': 20}
    Cells.add_pop(new_herb=[h1])
    b_w = Cells.herbs[0].weight
    Cells.feed_herbivore()
    a_w = Cells.herbs[0].weight
    assert b_w < a_w


@pytest.mark.parametrize("Cells", [c1, c2, c3])
def test_feed_carnivore(mocker, Cells):
    Cells.add_pop([{'species': 'Herbivore', 'age': 3, 'weight': 3} for _ in range(10)])
    ca1 = {'species': 'Carnivore', 'age': 3, 'weight': 10}
    Cells.add_pop(new_carn=[ca1])
    b_w = Cells.carns[0].weight
    mocker.patch("random.random", return_value=0.0001)
    Cells.feed_carnivore()
    a_w = Cells.carns[0].weight
    assert b_w < a_w




@pytest.mark.parametrize("Cells", [c1, c2, c3])
def test_annual_weight_loss(Cells):
    Cells.add_pop([{'species': 'Herbivore', 'age': 3, 'weight': 3}])
    b_w = Cells.herbs[0].weight
    Cells.annual_weight_loss()
    a_w = Cells.herbs[0].weight
    assert b_w - (b_w * Cells.herbs[0].default_parameters['eta']) == a_w


@pytest.mark.parametrize("Cells", [c1, c2, c3])
def test_annual_birth(mocker, Cells):
    Cells.add_pop([{'species': 'Herbivore', 'age': 3, 'weight': 50} for _ in range(10)])
    mocker.patch("random.random", return_value=0.0001)
    bc = Cells.animal_count()[2]
    Cells.annual_birth()
    ac = Cells.animal_count()[2]
    assert bc < ac


@pytest.mark.parametrize("Cells", [c1, c2, c3])
def test_annual_death(mocker, Cells):
    Cells.add_pop([{'species': 'Herbivore', 'age': 3, 'weight': 3} for _ in range(10)])
    mocker.patch("random.random", return_value=0.0001)
    bd = Cells.animal_count()
    Cells.annual_death()
    ad = Cells.animal_count()
    assert bd > ad



