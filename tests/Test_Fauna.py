import biosim.Fauna as Fauna
import math
import pytest
import random

"Test subjects"
h1 = Fauna.Herbivores()
c1 = Fauna.Carnivores()
h2 = Fauna.Herbivores()
c2 = Fauna.Carnivores()
h3 = Fauna.Herbivores()
c3 = Fauna.Carnivores()


def test_set_params():
    default_parameters1 = {'w_birth': 8.0, 'sigma_birth': 1.5,
                           'beta': 0.9, 'eta': 0.05,
                           'a_half': 40.0, 'phi_age': 0.6,
                           'w_half': 10.0, 'phi_weight': 0.1,
                           'mu': 0.25, 'gamma': 0.1,
                           'zeta': 3.5, 'xi': 1.2,
                           'omega': 0.4, 'F': 10.0
                           }

    herbivores = Fauna.Herbivores()
    herbivores.set_params(Fauna.Herbivores.default_parameters)
    assert herbivores.default_parameters == default_parameters1


def test_set_params_c():
    default_parameters2 = {'w_birth': 6.0, 'sigma_birth': 1.0,
                           'beta': 0.75, 'eta': 0.125,
                           'a_half': 40.0, 'phi_age': 0.3,
                           'w_half': 4.0, 'phi_weight': 0.4,
                           'mu': 0.4, 'gamma': 0.8,
                           'zeta': 3.5, 'xi': 1.1,
                           'omega': 0.8, 'F': 50.0, 'DeltaPhiMax': 10
                           }
    carnivores = Fauna.Carnivores()
    carnivores.set_params(Fauna.Carnivores.default_parameters)
    assert carnivores.default_parameters == default_parameters2


@pytest.mark.parametrize("Animals", [h1, c1])
def test_has_moved(Animals):
    """
    tests whether the animal has moved
    :param Animals: herbivore, carnivore
    :assert: tests
    """
    Animals.has_moved()
    assert Animals.moved == True


@pytest.mark.parametrize("Animals", [h1, c1])
def test_reset_moved(Animals):
    "Tests if the reset_moved attribute works as intended"
    Animals.reset_moved()
    assert Animals.moved == False


@pytest.mark.parametrize("Animals", [h1, c1])
def test_ages(Animals):
    Animals.ages()
    assert Animals.age == 1


@pytest.mark.parametrize("Animals", [h1, c1])
def test_birth_weight(Animals):
    "Checks if the birth_weight is within the Gaussian distribution"
    Animals.birth_weight()
    assert 1.96 <= Animals.birth_weight() <= 14


@pytest.mark.parametrize("Animals", [h1, c1])
def test_gain_weight(Animals):
    "Tests if the gain_weight attribute adds weight to the animal"
    w1 = Animals.weight
    Animals.gain_weight(2)
    w2 = Animals.weight
    assert w1 + (2 * Animals.default_parameters["beta"]) == w2


@pytest.mark.parametrize("Animals", [h1, c1])
def test_lose_weight(Animals):
    "Tests if the lose_weight attribute makes the animal lose weight"
    w1 = Animals.weight
    Animals.lose_weight()
    w2 = Animals.weight
    assert w1 - (w1 * Animals.default_parameters["eta"]) == w2


def test_fitness():
    "Tests if the animals fitness follows the given calculation"
    h2.set_params(Fauna.Herbivores.default_parameters)
    q_plus = 1 / (1 + math.exp(0.6 * (0 - 40)))
    q_minus = 1 / (1 + math.exp(-0.1 * (h2.weight - 10)))
    assert h2.phitness == q_plus * q_minus


def test_fitness_c():
    c2.set_params(Fauna.Carnivores.default_parameters)
    q_plus = 1 / (1 + math.exp(0.3 * (0 - 40)))
    q_minus = 1 / (1 + math.exp(-0.4 * (c2.weight - 4)))
    assert c2.phitness == q_plus * q_minus


@pytest.mark.parametrize("Animals", [h1, c1])
def test_death(Animals):
    "Tests if the animal dies when weight reaches zero"
    Animals.weight = 0
    assert Animals.death() == True


@pytest.mark.parametrize("Animals", [h1, c1])
def test_birth(Animals):
    "Tests if there are no other of the same species will"
    "the birth function return -None- as intended"
    assert Animals.birth(0) == None

@pytest.mark.parametrize("Animals", [h3, c3])
def test_moves(Animals, mocker):
    Animals.default_parameters = {"mu": 0.5}
    Animals.phitness = 0.9
    mocker.patch("random.random", return_value=0.0001)
    assert Animals.moves() == True




