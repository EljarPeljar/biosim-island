import biosim.celle as celle
import biosim.Fauna as Fauna
import biosim.Islands as Island
import pytest
import numpy as np

test_geogr = """\
               WWW
               WLW
               WWW"""

not_surround_geogr = """\
               WWWW
               LLLL
               WWWW"""

not_cons_row_geogr = """\
               WWWW
               WLW
               WWWW"""

not_supported_value_geogr = """\
               WWWW
               WLXW
               WWWW"""

migration_geogr = """\
                WWWWW
                WWLWW
                WLLLW
                WWLWW
                WWWWW"""

I1 = Island.Island(test_geogr)
I2 = Island.Island(test_geogr)
I3 = Island.Island(test_geogr)
I4 = Island.Island(migration_geogr)
I5 = Island.Island(test_geogr)
I6 = Island.Island(test_geogr)
I7 = Island.Island(migration_geogr)
I8 = Island.Island(migration_geogr)


@pytest.mark.parametrize("Islands",
                         [not_supported_value_geogr, not_cons_row_geogr, not_surround_geogr])
def test_island_geography_prequisites(Islands):
    # Checks that a ValueError is raised when creating invalid islands
    with pytest.raises(ValueError):
        Island.Island(Islands)


def test_island_geography():
    I1_dict = {(1, 1): 'W',
               (1, 2): 'W',
               (1, 3): 'W',
               (2, 1): 'W',
               (2, 2): 'L',
               (2, 3): 'W',
               (3, 1): 'W',
               (3, 2): 'W',
               (3, 3): 'W'}
    assert I1.island_geography() == I1_dict


def test_add_island_pop():
    pop = [{"loc": (2, 2),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 8}]
            },
           {"loc": (2, 2),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 8}]
            }]
    I1.add_island_pop(pop)
    # Checks Herbivore and Carnivore population
    assert len(I1.isle[(2, 2)].herbs) == 1
    assert len(I1.isle[(2, 2)].carns) == 1


def test_island_feed_all(mocker):
    pop = [{"loc": (2, 2),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 8} for _ in range(10)]
            },
           {"loc": (2, 2),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 40}]
            }]
    I2.add_island_pop(pop)
    bh = I2.isle[(2, 2)].herbs[0].weight
    bc = I2.isle[(2, 2)].carns[0].weight
    mocker.patch("random.random", return_value=0.0001)
    I2.island_feed_all()
    ah = I2.isle[(2, 2)].herbs[0].weight
    ac = I2.isle[(2, 2)].carns[0].weight
    assert bh < ah
    assert bc < ac


def test_island_procreation(mocker):
    pop = [{"loc": (2, 2),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 30} for _ in range(10)]
            },
           {"loc": (2, 2),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 30} for _ in range(10)]
            }]
    I3.add_island_pop(pop)
    mocker.patch("random.random", return_value=0.0001)
    nb = I3.number_of_animals()
    I3.island_procreation()
    na = I3.number_of_animals()
    assert nb < na


def test_neighbours():
    # Tests if the correct neighbouring cells are returned
    assert I1.neighbours([2, 2]) == [(2, 3), (2, 1), (1, 2), (3, 2)]


def test_migration():
    I4.add_island_pop([
        {'loc': (3, 3),
         'pop': [{'species': 'Herbivore', 'age': 3, 'weight': 30} for _ in range(5)]},
        {'loc': (3, 3), 'pop': [{'species': 'Carnivore', 'age': 3, 'weight': 30} for _ in range(5)]}
    ])
    for _ in range(5):
        I4.isle[(3, 3)].carns[_].moves = lambda: True
    for _ in range(5):
        I4.isle[(3, 3)].herbs[_].moves = lambda: True
    I4.migration()
    assert len(I4.isle[(3, 3)].carns) == 0
    assert len(I4.isle[(3, 3)].herbs) == 0


def test_island_aging():
    pop = [{"loc": (2, 2),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 30}]
            },
           {"loc": (2, 2),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 30}]
            }]
    I3.add_island_pop(pop)
    assert I3.isle[(2, 2)].carns[0].age == 3 and I3.isle[(2, 2)].carns[0].age == 3
    I3.island_aging()
    assert I3.isle[(2, 2)].herbs[0].age == 4 and I3.isle[(2, 2)].herbs[0].age == 4


def test_island_weightloss():
    pop = [{"loc": (2, 2),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 30} for _ in range(5)]
            },
           {"loc": (2, 2),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 30} for _ in range(5)]
            }]
    I3.add_island_pop(pop)
    wb = []
    for _ in range(5):
        wb.append(I3.isle[(2, 2)].carns[_].weight)
        wb.append(I3.isle[(2, 2)].herbs[_].weight)
    I3.island_weightloss()
    wa = []
    for _ in range(5):
        wa.append(I3.isle[(2, 2)].carns[_].weight)
        wb.append(I3.isle[(2, 2)].herbs[_].weight)
    assert wb > wa


def test_island_death():
    pop = [{"loc": (2, 2),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 0} for _ in range(5)]
            },
           {"loc": (2, 2),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 0} for _ in range(5)]
            }]
    I3.add_island_pop(pop)
    ab = I3.number_of_animals()
    I3.island_deaths()
    aa = I3.number_of_animals()
    assert ab > aa


def test_one_cell_count():
    pop = [{"loc": (2, 2),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 10} for _ in range(7)]
            },
           {"loc": (2, 2),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 10} for _ in range(4)]
            }]
    I5.add_island_pop(pop)
    expected = np.array([[0., 0., 0.],
                         [0., 11., 0.],
                         [0., 0., 0.]])
    expected_h = np.array([[0., 0., 0.],
                           [0., 7., 0.],
                           [0., 0., 0.]])
    expected_c = np.array([[0., 0., 0.],
                           [0., 4., 0.],
                           [0., 0., 0.]])
    assert np.array_equal(I5.one_cell_count()[0], expected)
    assert np.array_equal(I5.one_cell_count()[1], expected_h)
    assert np.array_equal(I5.one_cell_count()[2], expected_c)


def test_number_of_animals():
    pop = [{"loc": (2, 2),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 10} for _ in range(22)]
            },
           {"loc": (2, 2),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 10} for _ in range(12)]
            }]
    assert I6.number_of_animals()[2] == 0
    I6.add_island_pop(pop)
    assert I6.number_of_animals()[0] == 22
    assert I6.number_of_animals()[1] == 12
    assert I6.number_of_animals()[2] == 34


def test_island_reset_moved():
    pop = [{"loc": (3, 3),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 40} for _ in range(2)]
            },
           {"loc": (3, 3),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 40} for _ in range(2)]
            }]
    I7.add_island_pop(pop)
    I7.isle[(3, 3)].herbs[0].moved = True
    I7.isle[(3, 3)].herbs[1].moved = True
    I7.isle[(3, 3)].carns[0].moved = True
    I7.isle[(3, 3)].carns[1].moved = True
    assert I7.isle[(3, 3)].herbs[0].moved == True
    assert I7.isle[(3, 3)].herbs[1].moved == True
    assert I7.isle[(3, 3)].carns[0].moved == True
    assert I7.isle[(3, 3)].carns[1].moved == True
    I7.island_reset_moved()
    assert I7.isle[(3, 3)].herbs[0].moved == False
    assert I7.isle[(3, 3)].herbs[1].moved == False
    assert I7.isle[(3, 3)].carns[0].moved == False
    assert I7.isle[(3, 3)].carns[1].moved == False

def test_year_count():
    assert I2.year == 0
    I2.year_count()
    assert I2.year == 1


def test_sim_one_year():
    "Have tested all methods inside the sim one year"
    bt = I8.year
    I8.sim_one_year()
    at = I8.year
    assert bt < at





