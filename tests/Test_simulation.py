import biosim.Fauna as Fauna
import pytest
import biosim.simulation as sim
import biosim.celle as cell

geogr = """\
                WWWWW
                WWLWW
                WLLLW
                WWLWW
                WWWWW"""


"""Test Subjects"""
h1 = Fauna.Herbivores()
c1 = Fauna.Carnivores()
cell1 = cell.Lowland()
bisim = sim.BioSim(geogr, ini_pop=[], seed=12)
bisim2 = sim.BioSim(geogr, ini_pop=[], seed=12)
bisim3 = sim.BioSim(geogr, ini_pop=[], seed=12)

pop = [{"loc": (3, 3),
        "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 8}]
        },
       {"loc": (3, 3),
        "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 8}]
        }]
bisim3.add_population(pop)

def test_set_animal_parameters():
    default_parameters1 = {'w_birth': 8.0, 'sigma_birth': 1.5,
                           'beta': 0.9, 'eta': 0.05,
                           'a_half': 40.0, 'phi_age': 0.6,
                           'w_half': 10.0, 'phi_weight': 0.1,
                           'mu': 0.25, 'gamma': 0.1,
                           'zeta': 3.5, 'xi': 1.2,
                           'omega': 0.4, 'F': 10.0
                           }
    sim.BioSim.set_animal_parameters(bisim, species="Herbivore", params=default_parameters1)
    assert h1.default_parameters == default_parameters1

def test_set_landscape_parameters():
    default_params_cells = {'f_max': 800}
    sim.BioSim.set_landscape_parameters("H", default_params_cells)
    assert cell1.default_params_cell == default_params_cells

def test_simulate():
    bisim.simulate(5)
    expected_sim_calls = 5
    assert bisim.isl.year == expected_sim_calls

def test_add_population():
    pop = [{"loc": (3, 3),
            "pop": [{'species': 'Herbivore', 'age': 3, 'weight': 8}]
            },
           {"loc": (3, 3),
            "pop": [{'species': 'Carnivore', 'age': 3, 'weight': 8}]
            }]
    bisim.add_population(pop)
    assert bisim.num_animals_per_species == {'Herbivore': 1, 'Carnivore': 1}

def test_year():
    sim_years = 4
    bisim2.simulate(sim_years)
    assert bisim2.year == sim_years

def test_num_animals():
    assert bisim3.num_animals == 2

def test_num_animals_per_species():
    assert bisim3.num_animals_per_species == {'Herbivore': 1, 'Carnivore': 1}

def test_total_num_herbivores():
    assert bisim3.total_num_herbivores() == 1

def test_total_num_carnivores():
    assert bisim3.total_num_carnivores() == 1

def test_get_population():
    assert bisim3.get_population() == ([[0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0]],
    [[0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0]])

def test_get_fitness():
    """Checks if we get the fitness, the values in assert are the base fitness for added pop"""
    assert bisim3.get_fitness() == ([0.4501660025847122], [0.8320058115926796])

def test_get_age():
    assert bisim3.get_age() == ([3], [3])

def test_get_weight():
    assert bisim3.get_weight() == ([8], [8])





